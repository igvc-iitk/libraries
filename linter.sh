#!/bin/sh
echo "Start Linting.."
find . -type f -regex ".*\.\(cpp\|h\)" > /tmp/files
cat /tmp/files | xargs ./cpplint --filter=-legal/copyright 2>&1 | grep "found: 0"
