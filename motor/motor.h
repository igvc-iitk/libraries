/* library tested 30 NOVEMBER
   Made by Deep Goel and Kartik Gupta
*/
#ifndef  MOTOR_MOTOR_H_
#define  MOTOR_MOTOR_H_
//
// (imp) motor will be considered cw if dir1 is HIGH and DIR2 is LOW
//


#ifndef COUNT_CONST
#define COUNT_CONST 3

#endif  // COUNT_CONST
class Motor {
// Variables
 public:
  int dir1_pin;    // stores pin no of direction1
  int dir2_pin;    // stores pin no of direction2
  int pwm_pin;     // stores pin no of pwm pin
  int dir1;        // stores value of DIR1PIN as 1 or 0
  int dir2;        // stores value of DIR2PIN as 1 or 0
  int pwm;         // pwm: Stores the pwm value given to the motor
  int mean_speed;  // mean_speed: The value to which motor moves when speed=100%
  float speed;     // speed     : Speed of the motor in percentage of meanspeed
  int damping;     // to be changed later by trial
// Functions
 public:
  // constructor
  Motor();
  // constructor with attachments of pins
  Motor(int Dir1, int Dir2, int Pwm);
  // attachments of pins
  void attachMotor(int Dir1, int Dir2, int Pwm);
  // +ve for CW and -ve for CCW.
  void moveMotor(int Pwm);
  // dir1 and dir2 can be 1 or 0,pwm can only be +ve for CW
  void moveMotor(int Dir1, int Dir2, int Pwm);
  // By default stop motor will lock motor
  void stopMotor();
  // To lock the motor
  void lockMotor();
  // Free the motor
  void freeMotor();
  // Sets the meanspeed with which motor moves when speed=100%
  void setMeanSpeed(int Speed);
  // +ve for CW and -ve for CCW. Speed in percentage of meanspeed
  void setMotorSpeed(int Speed);
  // dir1 and dir2 can be 1 or 0
  void setMotorSpeed(int Dir1, int Dir2, int Speed);
  // Just to change the PWM in whatever direction the motor was moving
  void changePWM(int Pwm);
  // Just to change the speed (In percentage) not the direction
  void changeSpeed(int Speed);
  // +1 for cw and -1 for ccw and 0 for free or locked
  int getDirection();
  // +1 for free and 0 for not free
  int isFree();
  // +1 for locked and 0 for not locked
  int isLocked();
  // returns speed in % of mean speed
  int getSpeed();
  // returns +ve for CW and -ve for CCW.
  int getPWM();
  // +ve for CW and -ve for CCW.
  void startSmoothly(int Speed);
  void stopSmoothly();
};
#endif    // MOTOR_MOTOR_H_
