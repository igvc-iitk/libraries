This is the libraries repo for all the arduino libraries created over the last
few years. This can be installed for easy arduino access by executing the
following command inside your Arduino folder:

```
git clone https://gitlab.com/teamrobocon-iitk/libraries.git
```