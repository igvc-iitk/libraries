All code in these libraries should follow [Google C++ guidelines](https://google.github.io/styleguide/cppguide.html)
preferably.

Salient points noted here:
* All header files should have `#define` guards to prevent multiple inclusion. The format of the symbol name should be `<PROJECT>_<PATH>_<FILE>_H_`.
* Use standard order for readability and to avoid hidden dependencies: Related header, C library, C++ library, other libraries' `.h`, your project's `.h`
* Use only spaces, and indent 2 spaces at a time. We use spaces for indentation.
  Do not use tabs in your code. You should set your editor to emit spaces when
  you hit the tab key. Contact @yash111998 for more info.

Alright this is the complete guide to how you would add something new to
these libraries:
* Fork this repo - There is a fork button somewhere at the top left
* This creates a copy of this repo under your account
* now remove the libraries folder if you already have one and clone
  this new one (the one under your account)
* Make changes, add files etc.
* Check lint by running `linter.sh` - Ask for help if required
* Once there are no lint errors, run `git add .` - notice the `.`
* Check the files added to ensure you haven't added any my mistake -
  `git status`
* Commit with a meaningful commit message - `git commit -m "Very Meaningful
  Message"`
* Now all the changes you have done are local... They aren't available
  online
* Send it online using `git push origin <Branchname>` - Usually the
  branch name is `master`
* Now check out your repo online -
  `https://gitlab.com/<username>/libraries.git`
* Create a "pull request" to the original repo - Ask @yash111998 or
  @abhibhav
* Wait for it to be accepted
